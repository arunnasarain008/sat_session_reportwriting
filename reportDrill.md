## *Project Analysis Report*

##### **Addressing Performance**:

Load balancers detect the health of back end resources and do not send traffic to servers that are not able to fulfill the request. Depending on our needs load balancers can be software-based or hardware-based. 

Hardware-based load balancers use high-performance hardware that can handle high traffic for the applications. Software-based load balancers can give an almost equivalent performance without the load balancing hardware.
 
The load balancers are most commonly used in level 4 and 7 of the OSI model based on the project. 

- Layer 4 load balancers work at the transport level. That means they can make routing decisions based on the TCP or UDP ports that packets use along with their source and destination IP addresses.
- Layer 7 load balancers act at the application level, the highest in the OSI model. They can evaluate a wider range of data than L4 counterparts, including HTTP headers and SSL session IDs, when deciding how to distribute requests across the server farm.

Load balancing is more efficient at L7 than L4, due to the added context in understanding and processing client requests to servers. Global server load balancing (GSLB) can extend the capabilities of either type across multiple data centers so that large volumes of traffic can be efficiently distributed and so that there will be no degradation of service for the end-user.

##### *Reference:* [Load Balancers](https://www.citrix.com/en-in/glossary/load-balancing.html)

#### **Addressing scalability**:

With vertical scaling (a.k.a. “scaling up”), you’re adding more power to your existing machine. In horizontal scaling (a.k.a. “scaling out”), you get the additional resources into your system by adding more machines to your network, sharing the processing and memory workload across multiple devices.

Generally scaling out is more desirable than scaling up since scaling out is adding more machines to the existing pool which increases the performance of the application whereas scaling up is adding more features to the existing machine which may even sometime lead to overload to the machines

##### *Reference:* [Horizontal and Vertical Scaling](https://www.missioncloud.com/blog/horizontal-vs-vertical-scaling-which-is-right-for-your-app)
